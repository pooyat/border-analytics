Solution to Border Analytics
============================

This is my solution to the Border Analytics [problem][1] in Python.

[1]: https://github.com/InsightDataScience/border-crossing-analysis

Run
---

Type

```bash
./run.sh
```
to read the input file from the `./input` directory, process it, and
write a report file in the `./output` dictionary.

Performance
-----------

The program creates a report in approximately six seconds for a large
input
[file](https://data.transportation.gov/api/views/keg4-3bc2/rows.csv?accessType=DOWNLOAD)
with over 350,000 rows on a machine with an AMD EPYC 7551 32-core
Processor, 2 threads per core, 2000 MHz CPU, and 32 GB Memory.

Approach
--------

Read each crossings record from the input file and make a list of
crossings by sorting them in order of date, measure, and border.  As
we try to add a record to the list, merge the record with a possibly
similar record in the list that has the same date, measure, and
border by tallying up the values of the two records.

It is rather fast to use binary (bisection) search to find a possibly
similar record in the list for a given input record because the list
is kept sorted as we read and add each record from the input file.

Binary search takes O(log_2 n) comparisons as opposed to linear search
which takes O(n) comparisons.  Note that, after search, we still need
to insert the input record in the list which will take O(n) movements
anyway.

After merging all similar records, we need to sort our list in a
slightly different order according to date, value, measure, and
border.

After the list is sorted, we compute the running monthly average.

Design
------

Following an object-oriented design, we created two different record
objects: `Record` to be used when reading and merging similar records
into a sorted list called *crossings list* and `Report_record` which is
a descendant of `Record` and to be used for sorting the crossings list
for the second time into a *report list*.

`Record` objects are used to read the input file and merge similar
rows based on date, measure, and border.  `Record`s are compared
accordingly by overloading the `<` and the `=` operators.

`Report_record` objects are used to sort the list of records based on
date, value, measure, and border by overloading the `<` operator
accordingly.  No two `Report_record`s will have the same date,
measure, and border because we already merged such `Record`s.  Hence,
no need to override the `=` operator.

A benefit of the object-oriented approach is that we do *not* need to
write a new search method every time that we want to compare the
records based on a different criterion.  We use the same
implementation of the search algorithm for different classes of
crossing records, namely `Recrod` and `Report_record`, by overloading
the `<` operator for each class accordingly that allows us to search
for and compare records based on different comparison criteria.

Although we could easily write our own code for the binary (bisection)
search algorithm, we used the Python `bisection` module which may have
the advantage of being faster.  The `bisection` module can overwrite
(see line 70 of the [source
code](https://github.com/python/cpython/blob/3.8/Lib/bisect.py)) the
Python implementation with an underlying C implementation of the
algorithm which tends to be faster.

We sorted our data structures, `crossings.crossings` and
`crossings.report` which are Python lists, in *ascending* order to be
able to take advantage of the built-in `bisection` library.  We, then,
read the report list in *reverse* order to print out the report file
in *descending* order.

The two data structures that we used, the crossings list and the report
list, contain almost the same contents sorted in slightly different
ways.  However, we need not be worried about their memory footprint as
each of these lists have nearly 7000 rows which is equivalent to about
350 KB.[^1]

We could use a number of heuristics to improve the already great
performance of the program only to add more complexity to the algorithm
and the implementation.  So we didn't.  But just for the record, we
could use the fact that the input file is almost already sorted
according to date.

All source files named with a `test_` prefix are unit tests which are
not necessary to the execution of the program.

Pseudocode
----------

```
create an empty crossings list
for each row of input
    read border, date, measure
    (assume the crossings list is sorted according to date, measure, and border)
    search (binary search) for date, measure, and border in the list
    if a similar row is found in the list
        increment output value by input value
    else put new row in proper position already discovered due to search
        (such that we keep the list sorted)
	
create an empty report list
for each row of the crossings list
    read border, date, measure, and value
    (assume the report list is sorted according to date, value, measure, and border)
    Using binary search put the row in the proper position in the report list
    (such that we keep the report list sorted)

compute running monthly average
    create an empty dictionary of {(border, measure): [count, sum]}
    for each row of the report list
        read measure and border
        search the dictionary for (measure, border)
            if found
                average (in the report list) = sum / count
                sum (in the dictionary) += value
                count (in the dictionary) += 1
            else
                add an entry of {(border, measure), [sum = value, count = 1]}
                (to the dictionary)
```
	
Exception Handling
------------------

If input file doesn't exist, an error message will be displayed.  Rows
of input table with missing or corrupt fields are skipped and a
warning message is shown for each skipped row.  The program can
process an input file with or without a header line.

Run Tests
---------

Type

```bash
cd insight_testsuite
./run_tests.sh
```

Run Unit Tests
--------------

Type

```bash
cd src
./run_unit_tests.sh
```

#### Footnotes

[^1]: A crossings or a report list contains around 7000 records
because just by looking at the data, we know that it contains
crossings of 24 years (2019 - 1996 + 1 = 24) where each year is 12
months, and for each date (combination of year and month) we have
around 12 measures and 2 borders.  Hence, 6912 records (24 * 12 * 12 *
2 = 6912).  If each record contains nearly 50-byte worth of data, a
Report object will be as large as 50 * 7000 bytes which is 350,000
bytes or nearly 350 KB or 0.35 MB that can easily be stored and looked
up fast for individual records in today's personal computers.
