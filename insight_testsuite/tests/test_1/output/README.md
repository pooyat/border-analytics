`crossings.csv` is created to be used in a unit test, not integration
tests.  In particular it is used in unit test for Crossings.add_record
method (the unit test method is called
test_crossings.test_add_record).
