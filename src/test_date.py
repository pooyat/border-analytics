import unittest
import date


class Test_date(unittest.TestCase):
    """Test Date object in date module"""

    def setUp(self):
        """Introduce a date that may be used in ANY of the test functions in
           this class"""
        self.date_1 = date.Date("06/01/2019 12:00:00 AM")
        self.date_2 = date.Date("05/01/2019 12:00:00 AM")
        self.date_3 = date.Date("06/30/1996 12:00:00 AM")
        self.date_4 = date.Date("06/12/2019 12:00:00 AM")

    def test_convert_date(self):
        """Test if date is converted correctly"""
        self.assertEqual(date.convert_date("06/01/2019 12:00:00 AM"), 201906)
        with self.assertRaises(ValueError):
            date.convert_date("xx/xx/xxxx 12:00:00 AM")

    def test_init(self):
        """Test if Date object is created correctly"""
        self.assertEqual(self.date_1.year_month, 201906)
        with self.assertRaises(ValueError):
            date.Date("xx/xx/xxxx 12:00:00 AM")

    def test_lt(self):
        """Test if < operator is overloaded correctly for Date object"""
        self.assertEqual(self.date_1 < self.date_2,
                         201906 < 201905)
        self.assertFalse(self.date_1 < self.date_2)
        self.assertFalse(self.date_1 < self.date_3)
        self.assertFalse(self.date_2 < self.date_3)
        self.assertFalse(self.date_1 < self.date_4)

    def test_eq(self):
        """Test if == operator is overloaded correctly for Date object"""
        self.assertTrue(self.date_1 == self.date_4)
        self.assertFalse(self.date_1 == self.date_2)
        self.assertFalse(self.date_1 == self.date_3)
        self.assertFalse(self.date_2 == self.date_3)

    def test_str(self):
        """Test if string representation of Date object is as expected"""
        self.assertEqual(str(self.date_1), "06/01/2019 12:00:00 AM")

    def shortDescription(self):
        """Hide docstring of each unit test from test output"""
        return None
