# Utilities for processing CSV files

import csv


def remove_header(csvf):
    """Assume csvf is a file object representing a CSV file
       Return the same file object with any possible header removed"""
    if csv.Sniffer().has_header(csvf.read(256)):
        csvf.seek(0)
        csvf.readline()
    else:
        csvf.seek(0)
    return csvf
