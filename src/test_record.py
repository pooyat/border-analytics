import unittest
from record import Record
from date import Date


class Test_record(unittest.TestCase):
    """Test Record object in record module"""

    def setUp(self):
        """Introduce variables to be used in any of following test methods"""
        self.record_1 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "6483")
        self.record_2 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "3")
        self.record_3 = Record("US-Mexico Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "6483")
        self.record_4 = Record("US-Canada Border", "05/25/1997 12:00:00 AM",
                               "Truck Containers Full", "683")
        self.record_5 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Empty", "6483")
        self.record_6 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "6483")

    def test_init(self):
        """Test if Record object is created as expected"""
        self.assertEqual(self.record_1.get_border(), "US-Canada Border")
        self.assertEqual(self.record_1.date, Date("03/01/2019 12:00:00 AM"))
        self.assertEqual(self.record_1.measure, "Truck Containers Full")
        self.assertEqual(self.record_1.value, 6483)
        with self.assertRaises(ValueError):
            Record("US-Canada Border", "xx/xx/xxxx 12:00:00 AM",
                   "Truck Containers Full", "6483")
        with self.assertRaises(ValueError):
            Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                   "Truck Containers Full", "xxxx")

    def test_lt(self):
        """Test if < operator is overloaded as expected for Record object"""
        self.assertFalse(self.record_1 < self.record_2)
        self.assertFalse(self.record_2 < self.record_1)
        self.assertTrue(self.record_1 < self.record_3)
        self.assertTrue(self.record_2 < self.record_3)
        self.assertFalse(self.record_1 < self.record_4)
        self.assertFalse(self.record_1 < self.record_5)
        self.assertFalse(self.record_1 < self.record_6)

    def test_eq(self):
        """Test if == operator is overloaded as expected for Record object"""
        self.assertTrue(self.record_1 == self.record_2)  # r2 is equal to r1
        self.assertFalse(self.record_1 == self.record_3)
        self.assertFalse(self.record_1 == self.record_4)
        self.assertFalse(self.record_3 == self.record_4)
        self.assertFalse(self.record_1 == self.record_5)
        self.assertTrue(self.record_1 == self.record_6)

    def shortDescription(self):
        """Hide docstring of each unit test from test output"""
        return None
