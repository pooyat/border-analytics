from record import Record


class Report_record(Record):
    """A record to be displayed in report"""

    def __init__(self, record):
        """Create Report_record object from Record object"""
        border = record.get_border()
        date = record.get_date()
        measure = record.get_measure()
        value = record.get_value()
        Record.__init__(self, border, date, measure, value)
        self.average = 0
        
    def get_border(self):
        return self.border

    def get_date(self):
        return str(self.date)

    def get_measure(self):
        return self.measure

    def get_value(self):
        return self.value

    def set_averge(self, ave):
        self.average = ave

    def __lt__(self, other):
        return (self.date, self.value, self.measure, self.border) < \
            (other.date, other.value, other.measure, other.border)

    def __str__(self):
        return ",".join((self.border, str(self.date), self.measure,
                         str(self.value), str(self.average)))
