def convert_date(date):
    """Assume date is a string in this format: 06/01/2019 12:00:00 AM
       Return a date as an integer by concatenating a four-digit year
       and a two-digit month.  Converted dates can be compared more easily
       against each other."""
    try:
        return int(date[6:10] + date[0:2])
    except ValueError:
        raise


class Date(object):
    """Store date by year and month only. Dates can be compared based on
       historical precedence"""

    def __init__(self, date):
        """Assume date is a string in this format: 06/01/2019 12:00:00 AM.
           Create a date object based on year and month that can later
           on be compared easily based on historical precedence against 
           another date object."""
        self.year_month = convert_date(date)

    def __lt__(self, other):
        """Return True if self precedes other in historical order, and False
           otherwise"""
        return self.year_month < other.year_month

    def __eq__(self, other):
        """Return True if self represents the same year and month as other,
           and False otherwise"""
        return self.year_month == other.year_month

    def __str__(self):
        """Return self in the original format: 06/01/2019 12:00:00 AM"""
        month = self.year_month % 100
        month_string = str(month)
        if month // 10 == 0:
            month_string = "0" + month_string
        year = self.year_month // 100
        return month_string + "/01/" + str(year) + " 12:00:00 AM"
