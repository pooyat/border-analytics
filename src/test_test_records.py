from test_records import Records


def test_records():
    input_records = Records()
    infilename = "../insight_testsuite/tests/test_1/input/Border_Crossing_Entry_Data.csv"
    input_records.read(infilename, "input")
    print(input_records)

    output_records = Records()
    outfilename = "../insight_testsuite/tests/test_1/output/report.csv"
    output_records.read(outfilename, "output")
    print(output_records)


test_records()
