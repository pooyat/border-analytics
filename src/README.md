This directory contains the source code of the program.

All source files named with a `test_` prefix are unit tests which are
not necessary to the execution of the program.
