# This is the main script

import argparse
from crossings import Crossings

parser = argparse.ArgumentParser()
parser.add_argument("incsv", help = "input CSV file")
parser.add_argument("outcsv", help = "output CSV report")
args = parser.parse_args()

crossings = Crossings()
crossings.load_input(args.incsv)
crossings.report_sort()
crossings.running_average()
try:
    outfile = open(args.outcsv, 'w')
except IOError as err:
    print(err)
else:
    with outfile:
        outfile.write("Border,Date,Measure,Value,Average\n")
        outfile.write(str(crossings))
