import unittest
from report_record import Report_record
from record import Record


class test_report_record(unittest.TestCase):
    """Test report_record module"""

    def setUp(self):
        self.record_1 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "6483")
        self.record_2 = Record("US-Mexico Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "6483")
        self.record_3 = Record("US-Canada Border", "05/25/1997 12:00:00 AM",
                               "Truck Containers Full", "6483")
        self.record_4 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Empty", "6483")
        self.record_5 = Record("US-Canada Border", "03/01/2019 12:00:00 AM",
                               "Truck Containers Full", "3")
        self.rep_record_1 = Report_record(self.record_1)
        self.rep_record_2 = Report_record(self.record_2)
        self.rep_record_3 = Report_record(self.record_3)
        self.rep_record_4 = Report_record(self.record_4)
        self.rep_record_5 = Report_record(self.record_5)

    def test_init(self):
        """Test Report_record constructor"""
        self.assertEqual(self.rep_record_1.get_border(), "US-Canada Border")
        self.assertEqual(str(self.rep_record_1.get_date()),
                         "03/01/2019 12:00:00 AM")
        self.assertEqual(self.rep_record_1.get_measure(),
                         "Truck Containers Full")
        self.assertEqual(self.rep_record_1.get_value(), 6483)

    def test_lt(self):
        self.assertTrue(self.rep_record_1 < self.rep_record_2)
        self.assertFalse(self.rep_record_1 < self.rep_record_3)
        self.assertTrue(self.rep_record_3 < self.rep_record_2)
        self.assertTrue(self.rep_record_4 < self.rep_record_1)
        self.assertTrue(self.rep_record_5 < self.rep_record_1)
        
    def shortDescription(self):
        """Hide docstring of each unit test from test output"""
        return None
