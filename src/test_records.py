from record import Record
import csv
import csv_utility


class Records(object):
    """A list of records only in the order read from a file.  This class
       is written to test Crossings class."""

    def __init__(self):
        self.records = []
        self.contents = ""      # all file contents in a single string

    def get_records(self):
        """Yield the list of records one record at a time!"""
        for record in self.records:
            yield record
    
    def get_contents(self):
        return self.contents

    def read(self, filename, inout):
        """Assume filename is a file object containing records, and inout is a
           string which determines the file type 'input' or 'output'.
           Fill in the records list with all of records in the same
           order as the input file."""
        # output parameter may not be used
        # determine indices based on file type: input or output
        if inout == "input":
            idcs = (3, 4, 5, 6)
        elif inout == "output":
            idcs = (0, 1, 2, 3)
        try:
            infile = open(filename)
        except IOError as err:
            print(err)
        else:
            with infile:
                infile = csv_utility.remove_header(infile)
                input_table = csv.reader(infile)
                in_row_num = 0
                for row in input_table:
                    in_row_num += 1
                    try:
                        border, date, measure, value = row[idcs[0]], \
                            row[idcs[1]], row[idcs[2]], row[idcs[3]]
                        record = Record(border, date, measure, value)
                    except (IndexError, ValueError) as err:
                        print("Warning: input row number:", in_row_num, \
                              "was skipped", err)
                        continue
                    self.records.append(record)

    def read_output(self, outfilename):
        try:
            outfile = open(outfilename)
        except IOError as err:
            print(err)
        else:
            with outfile:
                outfile = csv_utility.remove_header(outfile)
                self.contents = outfile.read()

    def __str__(self):
        # Return a sting of records with commas in between and no trailing comma
        return "\n".join((str(record) for record in self.records))
