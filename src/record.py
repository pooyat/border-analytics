from date import Date


class Record(object):
    """A border crossing record. Each record has a unique combination of
       date, measure, and border. Records can be compared based on
       date, value, measure, and border."""
    # We strive to compare as few characters as possible when
    # comparing Records (or strings inside Records)

    def __init__(self, border, date, measure, value):
        """Assume border is a string, date is a Date, measure is a string,
           value is an int, and average is a float rounded to closes integer"""
        # Each combination of border, date, and measure forms a unique
        # record with values tallied up for possibly multiple INPUT
        # records with the same border, date, and measure.
        self.border = border
        self.date = Date(date)
        self.measure = measure
        try:
            self.value = int(value)
        except ValueError:
            raise

    def get_border(self):
        return self.border
        
    def get_date(self):
        """Return date in its original string format"""
        return str(self.date)
        
    def get_measure(self):
        return self.measure
        
    def get_value(self):
        return self.value
        
    def merge(self, other):
        """Assume self and other have same date, measure, and border
           Merge two records into one by tallying up their values"""
        self.value += other.value

    def __lt__(self, other):
        """Overload < operator to be used to sort records based on date,
           value, measure, and border, in that order"""
        return (self.date, self.measure, self.border) < \
            (other.date, other.measure, other.border)

    def __eq__(self, other):
        """Return True if self has the same date, measure, and border as
           other, and False otherwise"""
        return (self.date, self.measure, self.border) == \
            (other.date, other.measure, other.border)

    def __str__(self):
        """Return a string of border, date, measure, and value with a comma in
           between and no trailing comma"""
        return ",".join((self.border, str(self.date), self.measure,
                        str(self.value)))
