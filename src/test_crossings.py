import unittest
from crossings import Crossings
from test_records import Records


class Test_crossings(unittest.TestCase):
    """Test Crossings class"""
    # Modifying this test file may need convoluted mental maneuvers

    def setUp(self):
        """Introduce variables to be used in any of following test methods"""
        self.crossings_1 = Crossings()
        self.records_1 = Records()
        self.infilename_1 = "../insight_testsuite/tests/test_1/input/Border_Crossing_Entry_Data.csv"
        self.records_1.read(self.infilename_1, "input")
        self.outfilename_1 = "../insight_testsuite/tests/test_1/output/crossings.csv"
        self.records_1.read_output(self.outfilename_1)

        self.records_2 = Records()
        self.infilename_2 = "../insight_testsuite/tests/test_4/input/Border_Crossing_Entry_Data.csv"
        self.records_2.read(self.infilename_2, "input")
        self.outfilename_2 = "../insight_testsuite/tests/test_4/output/crossings.csv"
        self.records_2.read_output(self.outfilename_2)

    def test_init(self):
        """Test if Crossings constructor actually creates an empty list as a
           data attribute"""
        self.assertEqual(self.crossings_1.crossings, [])
        self.assertEqual(self.crossings_1.report, [])

    def test_add_record(self):
        """Test if records are added in sorted order to a Crossings list and
           if records with same date, measure, and border are correctly
           merged---their values tallied up"""
        # Read a file of records
        #     (into a Records object in same order as in file)
        # Add them to a Recordings object
        # Check if Recordings is as expected
        #     (Are Records sorted and matching records merged?)
        #     Read expected records into a Records object
        #     (Done using crossings.csv in test_1 and test_4 cases)
        crossings_1 = Crossings()
        for record in self.records_1.get_records():
            crossings_1.add_record(record)
        self.assertEqual(crossings_1.print_crossings(),
                         self.records_1.get_contents())

        crossings_2 = Crossings()
        for record in self.records_2.get_records():
            crossings_2.add_record(record)
        self.assertEqual(crossings_2.print_crossings(),
                         self.records_2.get_contents())

    def test_load_input(self):
        """Test if the crossings list returned by load_input is as expected"""
        self.crossings_1.load_input(self.infilename_1)
        self.assertEqual(self.crossings_1.print_crossings(),
                         self.records_1.get_contents())

        crossings_2 = Crossings()
        crossings_2.load_input(self.infilename_2)
        self.assertEqual(crossings_2.print_crossings(),
                         self.records_2.get_contents())

    def test_report_sort(self):
        """Test if a crossings report is sorted as expected according to date,
           value, measure, and border"""
        outfilename_1 = "../insight_testsuite/tests/test_1/output/report_sort.csv"
        self.records_1.read_output(outfilename_1)
        self.crossings_1.load_input(self.infilename_1)
        self.crossings_1.report_sort()
        self.assertEqual(str(self.crossings_1),
                         self.records_1.get_contents())

        outfilename_2 = "../insight_testsuite/tests/test_4/output/report_sort.csv"
        self.records_1.read_output(outfilename_2)
        crossings_2 = Crossings()
        crossings_2.load_input(self.infilename_2)
        crossings_2.report_sort()
        self.assertEqual(str(crossings_2),
                         self.records_1.get_contents())

    def shortDescription(self):
        """Hide docstring of all unit tests from test output"""
        return None
