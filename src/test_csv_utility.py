import unittest
import csv_utility


class Test_csv_utility(unittest.TestCase):

    def setUp(self):
        """Introduce files that may be used in ANY of the test functions in
           this class."""
        # infname1 contains a header, but infname2 does not.
        infname1 = "../insight_testsuite/tests/test_1/input/Border_Crossing_Entry_Data.csv"
        infname2 = "../insight_testsuite/tests/test_2/input/Border_Crossing_Entry_Data.csv"
        self.infname = [infname1, infname2]

    def test_remove_header(self):
        """Test if the same content is read from a file with a header and a
        file without one."""
        with open(self.infname[0]) as inf1, open(self.infname[1]) as inf2:
            out = inf2.read()
            inf2.seek(0)
            for inf in [inf1, inf2]:
                self.assertEqual(csv_utility.remove_header(inf).read(), out)

    def shortDescription(self):
        """Hide docstring of each unit test from test output"""
        return None
