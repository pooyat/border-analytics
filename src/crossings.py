from bisect import bisect_left, insort_left
from csv_utility import remove_header
from csv import reader
from record import Record
from report_record import Report_record


class Crossings(object):
    """A list of crossings sorted based on date, measure, and border"""
    # For each combination of date, measure, and border, there is at
    # most one Record in the Crossings list.

    def __init__(self):
        """Create empty Crossings list"""
        self.crossings = []
        self.report = []

    def add_record(self, record):
        """Assume record is of type Record.
           Add record in descending order to the Crossings list if
           record is new.  Otherwise, merge record with the existing
           record by tallying up their values."""
        # search for record
        #     if found (index of matching record is known)
        #         merge two records into one
        #     if not found (index of where to be inserted is known)
        idx = bisect_left(self.crossings, record)
        if idx != len(self.crossings) and self.crossings[idx] == record:
            self.crossings[idx].merge(record)
        else:
            self.crossings.insert(idx, record)
    
    def load_input(self, filename):
        """Assume filename is an input file.
           Load all records from filename in descending order to the
           Crossings list if record is new.  Otherwise, merge record
           with the existing record by tallying up their values."""
        try:
            infile = open(filename)
        except IOError as err:
            print(err)
        else:
            with infile:
                infile = remove_header(infile)
                input_table = reader(infile)
                row_num = 0
                for row in input_table:
                    row_num += 1
                    try:
                        border, date, measure, value = row[3], row[4], \
                            row[5], row[6]
                        record = Record(border, date, measure, value)
                    except (IndexError, ValueError) as err:
                        print("Warning: input row", row_num, "was skipped:", err)
                        continue
                    self.add_record(record)

    def report_sort(self):
        """Sort report records according date, value, measure, and border"""
        for record in self.crossings:
            rep_record = Report_record(record)
            insort_left(self.report, rep_record)

    def running_average(self):
        """Compute running monthly average for each report record"""
        # Two lookups at each iteration: first to see if there is a
        # key (measure, border) at all and second update the value
        # (sum, count)
	# create an empty dictionary of {(border, measure): [count, sum]}
	# for each row of the report list
	#     read measure and border
	#     search the dictionary for (measure, border)
	# 	  if found
	# 	      average (in the report list) = sum / count
	# 		  sum (in the dictionary) += value
	# 		  count (in the dictionary) += 1
	# 	      else
	# 		  add an entry of {(border, measure), [sum = value, count = 1]}
	# 		  (to the dictionary)
        cnt_sum = {}
        for rep_record in self.report:
            measure = rep_record.get_measure()
            border = rep_record.get_border()
            sum, count = 0, 0
            try:
                sum, count = cnt_sum[(measure, border)]  # first lookup
            except KeyError:
                pass
            else:
                rep_record.set_averge(int(sum / count + 0.5))
            finally:
                sum += rep_record.get_value()
                count += 1
                cnt_sum[(measure, border)] = [sum, count]  # second lookup

    def print_crossings(self):
        """Return a string of records sorted based on date, measure, and
           border with a newline character in between and no trailing
           newline character"""
        return "\n".join((str(record) for record in self.crossings))

    def __str__(self):
        """Return a string of records sorted based on date, value, measure,
           and border with a newline character in between and no
           trailing newline character"""
        return "\n".join((str(rep_record) for rep_record in reversed(self.report)))
